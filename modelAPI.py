from flask import Flask, request, Response
import numpy as np
from tensorflow.keras.models import load_model
import os
import json
import pandas as pd


'''
@author: Mohsen Shahriari

In this modelAPI script, the pretrained-model is loaded, and a prediction is calculated based on 
the received features through the API!


'''


def return_prediction(model, custom_df):

    mpg_predicted = []

    try:

        # parsing the JSON object!

        cylinders = custom_df['Cylinders']
        displacement = custom_df['Displacement']
        horsepower = custom_df['Horsepower']
        weight = custom_df['Weight']
        acceleration = custom_df['Acceleration']
        modelYear = custom_df['Model Year']
        cylinders = custom_df['Cylinders']
        europe = custom_df['Europe']
        japan = custom_df['Japan']
        usa = custom_df['USA']

        car_instance = [cylinders, displacement, horsepower, weight,
                        acceleration, modelYear, cylinders, europe, japan, usa]

        # using the model object to predict the car efficiency!
        mpg_predicted = model.predict(car_instance).flatten()

    except:

        print("There is a problem with the received JSON file or the features!")

    return mpg_predicted


app = Flask(__name__)


def ml_load_model():

    # loading the machine learning model in protobuf format!

    try:

        dnn_model = load_model(os.environ['MODEL_PATH'])

    except:

        raise(FileNotFoundError)

    return dnn_model

# 'D:\\Coding\\MetroMLEngineer\\mlengineer_task\\mlengineer_task\\dnn_model\\'


dnn_model = ml_load_model()


@app.route('/api/mpg', methods=['POST'])
def predict_mpg():

    content = request.json

    record = pd.DataFrame.from_dict(content, orient="index")

    results = return_prediction(model=dnn_model, custom_df=record)

    predictions = json.dumps({'predictions': results.tolist()})

    return Response(predictions, mimetype='application/json')


if __name__ == '__main__':
    
    
        app.run(debug=True, host='0.0.0.0', port=os.environ['SERVING_PORT'])  # 8002
 
