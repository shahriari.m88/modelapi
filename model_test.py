import unittest
from modelAPI import return_prediction
from tensorflow.keras.models import load_model
import numpy as np
import pandas as pd

'''
@author Mohsen Shahriair

Through this class, we test our functions.

Here, we test the return_prediction function to see if it's working ok, and if the correct model has been loaded!

'''


class TestModel(unittest.TestCase):

  

    def test_return_prediction(self):

        # Defining two example features from two cars

        car_examples = {

            "example1": {"Cylinders": 8.0,
                         "Displacement":    390.0,
                         "Horsepower":      190.0,
                         "Weight":         3850.0,
                         "Acceleration":     8.5,
                         "Model Year":        70.0,
                         "Europe":             0.0,
                         "Japan":              0.0,
                         "USA":              1.0},

            "example2": {"Cylinders": 9.0,
                         "Displacement":    490.0,
                         "Horsepower":      160.0,
                         "Weight":         3850.0,
                         "Acceleration":     8.5,
                         "Model Year":        70.0,
                         "Europe":             0.0,
                         "Japan":              0.0,
                         "USA":              1.0

                         }

        }

        # Converting the JSON file to a pandas dataframe

        test_car_examples = pd.DataFrame.from_dict(
            car_examples, orient="index")

        # Loading the model

        dnn_model = load_model(
            'D:\\Coding\\MetroMLEngineer\\mlengineer_task\\mlengineer_task\\dnn_model\\')

        # Performing the predictions

        results = return_prediction(
            model=dnn_model, custom_df=test_car_examples)

        # Assert if the values are equal to the already test values!    

        self.assertListEqual(results.tolist(),  [
                             124.69535827636719, 130.29351806640625])


if __name__ == '__main__':
    unittest.main()
