

# Base Image
FROM python:3.6

RUN apt-get update --allow-releaseinfo-change && apt-get install -y --no-install-recommends \
    tzdata \
    python3-setuptools \
    python3-pip \
    python3-dev \
    python3-venv \
    git \
    && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

ARG DEFAULT_PORT=8002

ENV MODEL_PATH="/ModelAPI/dnn_model/"

ENV SERVING_PORT=${DEFAULT_PORT}

WORKDIR /ModelAPI

COPY . /ModelAPI

RUN pip install -r requirements.txt

EXPOSE ${DEFAULT_PORT}

CMD ["python3", "modelAPI.py"]






